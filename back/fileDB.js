const fs = require("fs");
const fileName = "./db.json"
let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(fileName)
            data = JSON.parse(String(fileContents))
        } catch (e) {
            console.log(e)
            data = []
        }
    },
    getItems() {
        return data;
    },
    getItem(id) {
        const index = data.findIndex(item => item.id === id);
        if (index === -1) return {message: "Item not found"};
        return data[index];
    },
    save() {
        try {
            fs.writeFileSync(fileName, JSON.stringify(data))
        } catch (e) {
            console.log("Cannot write file");
        }
    },
    addItem(item) {
        data.push(item)
        this.save()
    },
    deleteItem(id) {
        const index = data.findIndex(item => item.id === id);
        if (index === -1) return {message: "Item not found"};
        const deletedItem = data.splice(index, 1);
        this.save();
        return deletedItem[0];
    },
    changeItem(id, item) {
        const index = data.findIndex(item => item.id === id);
        if (index === -1) return {message: "Item not found"};
        item.id = id;
        data[index] = item;
        this.save();
        return item
    }
};