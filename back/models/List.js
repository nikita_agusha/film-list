const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ListSchema = new Schema({
    nameList: {
        type: String,
        required: true
    },
    colFilm:{
        type:Number,
    }
});


const List = mongoose.model("List", ListSchema);
module.exports = List;