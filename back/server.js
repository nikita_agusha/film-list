const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config")
const list = require("./app/list")
const film = require("./app/film")
const bodyParser = require('body-parser');

const app = express();
const PORT = 8000;


app.use(cors());
app.use(express.static("public") );
app.use(bodyParser());

mongoose.connect(`${config.db.url}/${config.db.name}`, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("mongoose connected!");
        app.use("/list", list());
        app.use("/film", film());
        app.listen(PORT, () => {
            console.log("Server started at http://localhost:" + PORT);
        });
    });