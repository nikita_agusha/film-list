const express = require("express");
const Film = require("../models/Films");
const List = require("../models/List");
const router = express.Router();


const createRouter = (db) => {
    router.get("/", async (req, res) => {
        try {
            const film = await Film.find()
            res.send(film)
        } catch (e) {
            res.sendStatus(500);
        }
    });
    router.get("/:id", async (req, res) => {
        const list = await
            Film.find({list: req.params.id});
        res.send(list);
    });

    router.post("/", async (req, res) => {
        const film = new Film(req.body);
        await film.save();
        res.send(await Film.find({list: req.body.list}))
    })
    router.delete("/:id", async (req, res) => {
        const findInfo=await Film.find({_id:req.params.id})
        const deletedFilm = await Film.remove({_id: req.params.id});
        console.log( findInfo[0].list)
        res.send(await Film.find({list:findInfo[0].list}));
    });
    return router
}
module.exports = createRouter